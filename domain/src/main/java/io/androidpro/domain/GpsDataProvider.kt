package io.androidpro.domain

import kotlinx.coroutines.flow.Flow

interface GpsDataProvider{
    fun listenToGpsUpdates(): Flow<GpsUpdate>
}