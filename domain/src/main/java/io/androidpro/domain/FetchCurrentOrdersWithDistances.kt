package io.androidpro.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flow
import java.lang.Math.*

class FetchCurrentOrdersWithDistances(
    private val ordersRepository: OrdersRepository,
    private val gpsDataProvider: GpsDataProvider
){
    companion object {
        const val earthDistanceKm: Double = 6372.8 // in kilometers
    }

    suspend fun execute(): Flow<List<OrderWithDistance>> =
        // calculate distance to pickup or delivery
        // use Haversine formula
        // https://en.wikipedia.org/wiki/Haversine_formula
        // https://www.rosettacode.org/wiki/Haversine_formula#Kotlin

         combine(
            ordersRepository.getAll().convertToFlow(),
            gpsDataProvider.listenToGpsUpdates()
        ) { orders, gpsUpdate ->
              orders.map { order ->
                 val currentGpsLocation = when (gpsUpdate) {
                     is GpsUpdate.Error -> return@map OrderWithDistance(
                         order = order,
                         distanceToTarget = DistanceToTarget.NotAvailable
                     )

                     is GpsUpdate.GpsData -> Location(
                         lat = gpsUpdate.latitude,
                         long = gpsUpdate.longitude
                     )
                 }

                 val destinationLocation = when (order.orderStatus) {
                     OrderStatus.PLACED, OrderStatus.ACCEPTED,  -> Location(
                         order.restaurantAddress.latitude,
                         order.restaurantAddress.longitude
                     )

                     OrderStatus.CANCELLED, OrderStatus.DELIVERED -> null

                     OrderStatus.EN_ROUTE -> Location(
                         order.customerAddress.latitude,
                         order.customerAddress.longitude
                     )
                 }
                 val distanceToTarget =  when(destinationLocation) {
                     null -> DistanceToTarget.NotAvailable
                     else -> {
                         val distance = haversine(currentGpsLocation, destinationLocation)
                         DistanceToTarget.Available(Distance(distance))
                     }
                 }
                 OrderWithDistance(
                     order = order,
                     distanceToTarget = distanceToTarget
                 )
             }

        }


    private fun haversine(currentLocation: Location, destinationLocation: Location): Double {
        val λ1 = toRadians(currentLocation.lat) //lat1
        val λ2 = toRadians(destinationLocation.lat) //lat2
        val Δλ = toRadians(destinationLocation.lat - currentLocation.lat)
        val Δφ = toRadians(destinationLocation.long - destinationLocation.long)
        return 2 * earthDistanceKm * asin(sqrt(pow(sin(Δλ / 2), 2.0) + pow(sin(Δφ / 2), 2.0) * cos(λ1) * cos(λ2)))
    }
}

fun <E> E.convertToFlow(): Flow<E> {
    return flow {
        emit(this@convertToFlow)
    }
}

data class OrderWithDistance(
    val order: Order,
    val distanceToTarget: DistanceToTarget
)

@JvmInline
value class Distance(val value: Double)

sealed interface DistanceToTarget {
    class Available(val distance: Distance) : DistanceToTarget
    object NotAvailable : DistanceToTarget
}

class Location(val lat: Double, val long: Double)

