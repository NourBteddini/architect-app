package io.androidpro.architect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.androidpro.architect.OrderListState.Data
import io.androidpro.domain.FetchCurrentOrdersWithDistances
import io.androidpro.domain.OrderStatus
import io.androidpro.domain.OrdersRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.scan
import kotlinx.coroutines.launch

class OrderListViewModel(
    private val fetchCurrentOrdersWithDistances: FetchCurrentOrdersWithDistances,
    private val mapper: OrderWithDistanceListItemDisplayableMapper
) : ViewModel() {

    private val _state = MutableStateFlow<OrderListState>(OrderListState.Loading)
    val state: StateFlow<OrderListState>
        get() = _state.asStateFlow()

    init {
        loadData()
    }

    private fun loadData() {
        viewModelScope.launch {
            fetchCurrentOrdersWithDistances.execute().map { orderWitDistanceList ->
                orderWitDistanceList.map(mapper::map)
            }.onEach { items ->
                _state.value = Data(items)
            }.launchIn(viewModelScope)
        }
    }

}

sealed interface OrderListState {
    object Loading : OrderListState
    object Error : OrderListState
    data class Data(val orders: List<OrderListItemDisplayable>) : OrderListState
}

