package io.androidpro.architect

import io.androidpro.domain.OrderStatus
import java.util.Date

data class OrderListItemDisplayable(
    val orderId: String,
    val customerAddress: String,
    val restaurantName: String,
    val restaurantAddress: String,
    val orderItems: String,
    var orderStatus: OrderStatus,
    val pickupDate: Date,
    val distance: String
)