package io.androidpro.architect

import io.androidpro.domain.Address
import io.androidpro.domain.DistanceToTarget
import io.androidpro.domain.OrderWithDistance
import java.util.Date

class OrderWithDistanceListItemDisplayableMapper() {
    fun map(orderWithDistance: OrderWithDistance): OrderListItemDisplayable{
        return OrderListItemDisplayable(
            orderId = orderWithDistance.order.orderId,
            customerAddress = orderWithDistance.order.customerAddress.toReadableAddress(),
            orderItems = orderWithDistance.order.orderItems.joinToString(separator = ", ") { it.itemName },
            orderStatus = orderWithDistance.order.orderStatus,
            pickupDate = Date(orderWithDistance.order.timestamp),
            restaurantAddress = orderWithDistance.order.restaurantAddress.toReadableAddress(),
            restaurantName = orderWithDistance.order.restaurantName,
            distance = when (val result = orderWithDistance.distanceToTarget) {
                is DistanceToTarget.Available -> "Distance to pickup / deliver: ${result.distance.value}"
                DistanceToTarget.NotAvailable -> "Not Available"
            }
        )
    }

    private fun Address.toReadableAddress(): String {
        return this.address
    }
}